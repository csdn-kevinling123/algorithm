# -*- coding: utf-8 -*-


class Merge(object):
    '''
    归并排序-》先分再合
    1. 将数据进行分组知道不能再分为止
    2. 将数据进行两两合并，同时进行排序
    3. 重复步骤2
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        n = len(self.arr)
        self.merge_sort(0, n - 1)
        return self.arr

    def merge_sort(self, lo, hi):
        '''
        分组
        :param lo:
        :param hi:
        :return:
        '''
        if lo < hi:
            mid = lo + int((hi - lo) / 2)
            # 递归分组，直到不能再分为止
            self.merge_sort(lo, mid)
            self.merge_sort(mid + 1, hi)
            # 合并，排序
            self.merge(lo, mid, hi)

    def merge(self, l, m, r):
        '''
        合并
        :param l:
        :param m:
        :param r:
        :return:
        '''
        n1 = m - l + 1
        n2 = r - m

        # 创建临时数组
        L = [0] * (n1)
        R = [0] * (n2)

        # 拷贝数据到临时数组 arrays L[] 和 R[]
        for i in range(0, n1):
            L[i] = self.arr[l + i]

        for j in range(0, n2):
            R[j] = self.arr[m + 1 + j]

            # 归并临时数组到 arr[l..r]
        i = 0  # 初始化第一个子数组的索引
        j = 0  # 初始化第二个子数组的索引
        k = l  # 初始归并子数组的索引

        while i < n1 and j < n2:
            if L[i] <= R[j]:
                self.arr[k] = L[i]
                i += 1
            else:
                self.arr[k] = R[j]
                j += 1
            k += 1

        # 拷贝 L[] 的保留元素
        while i < n1:
            self.arr[k] = L[i]
            i += 1
            k += 1

        # 拷贝 R[] 的保留元素
        while j < n2:
            self.arr[k] = R[j]
            j += 1
            k += 1

    def less_(self, num1, num2):
        '''
        判断小于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 < num2


if __name__ == '__main__':
    arr = [8, 4, 5, 7, 1, 3, 6, 2]
    sort_ = Merge(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)