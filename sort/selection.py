# -*- coding: utf-8 -*-


class Selection(object):
    '''
    选择排序
    1. 默认组中的第一个元素是值最小的元素
    2. 该元素与中的其他元素进行比较，找到组中值最小的元素的下标
    3. 交换两个下标的值
    4. 依次循环类推
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        n = len(self.arr)
        for i in range(n-1):
            min_index = i
            for j in range(i+1, n):
                # 比较次数：(n-1) + (n-2) + (n-3) + 2 + 1 = n^2 / 2 - n / 2
                if self.greate_(num1=self.arr[min_index], num2=self.arr[j]):
                    min_index = j
            # 交换次数：n - 1
            self.exch_(i, min_index)
        # 总执行次数： ( n^2 / 2 - n / 2) + n - 1 = n^2 + n / 2 - 1
        # 根据大O推到法则，选择排序的时间复杂度为： O(n^2)
        return self.arr

    def greate_(self, num1, num2):
        '''
        判断大于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 > num2

    def exch_(self, index1, index2):
        '''
        交换位置
        :param arr:
        :param index1:
        :param index2:
        :return:
        '''
        temp = self.arr[index1]
        self.arr[index1] = self.arr[index2]
        self.arr[index2] = temp


if __name__ == '__main__':
    arr = [64, 34, 25, 12, 22, 11, 90]
    sort_ = Selection(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)