# -*- coding: utf-8 -*-


class Bubble(object):
    '''
    冒泡排序
    1. 第一个元素依次和组中的其余元素对比
    2. 大于则交换位置，否则不交换
    3. 循环完毕则第一元素就是最小的元素
    4. 以此类推，第二个元素再依次和后面的元素进行比较。。。
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        n = len(self.arr)
        for i in range(n):
            for j in range(0, n - i - 1):
                # 比较次数：(n-1) + (n-2) + (n-3) + 2 + 1 = n^2 / 2 - n / 2
                if self.greate_(num1=self.arr[j], num2=self.arr[j+1]):
                    # 交换次数：(n-1) + (n-2) + (n-3) + 2 + 1 =  n^2 / 2 - n / 2
                    self.exch_(j, j+1)
        # 总执行次数： ( n^2 / 2 - n / 2) + ( n^2 / 2 - n / 2) = n^2 - n
        # 根据大O推到法则，冒泡排序的时间复杂度为： O(n^2)
        return self.arr

    def greate_(self, num1, num2):
        '''
        判断大于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 > num2

    def exch_(self, index1, index2):
        '''
        交换位置
        :param arr:
        :param index1:
        :param index2:
        :return:
        '''
        temp = self.arr[index1]
        self.arr[index1] = self.arr[index2]
        self.arr[index2] = temp


if __name__ == '__main__':
    arr = [64, 34, 25, 12, 22, 11, 90]
    sort_ = Bubble(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)