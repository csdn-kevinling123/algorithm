# -*- coding: utf-8 -*-


class Shell(object):
    '''
    希尔排序-》插入排序的改良版
    1. 根据增长量对数据进行分组
    2. 将分好组是数据分别进行插入排序
    3. 减少增长量，最小为1，重复第二步操作
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        n = len(self.arr)
        # 增长量规则
        h = int(n / 2)

        while h >= 1:
            # 找到待插入从数据
            for i in range(h, n):
                # 把待插入的数据插入的对用的组中
                j = i
                while j >= h:
                    if self.greate_(self.arr[j-h], self.arr[j]):
                        self.exch_(index1=j-h, index2=j)
                    else:
                        break
                    j -= h

            # 增长量减少规则
            h = int(h / 2)

        return self.arr

    def greate_(self, num1, num2):
        '''
        判断大于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 > num2

    def exch_(self, index1, index2):
        '''
        交换位置
        :param arr:
        :param index1:
        :param index2:
        :return:
        '''
        temp = self.arr[index1]
        self.arr[index1] = self.arr[index2]
        self.arr[index2] = temp


if __name__ == '__main__':
    arr = [9, 1, 2, 5, 7, 4, 8, 6, 3, 5]
    sort_ = Shell(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)