# -*- coding: utf-8 -*-


class Quick(object):
    '''
    快速排序
    1. 设置一个分界值，将其分为两组
    2. 将比该值小的放左边，大的放右边
    3. 然后左边和右边分别继续设置一个分界值，重复第二步操作
    4. 分到不可再分后，排序即完成
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        lo = 0
        hi = len(self.arr) - 1
        self.quick_sort(lo, hi)
        return self.arr

    def quick_sort(self, lo, hi):
        '''
        排序
        :param lo:
        :param hi:
        :return:
        '''
        if lo < hi:
            partition = self.partition(lo, hi)
            # 左右部分分别递归排序
            self.quick_sort(lo, partition-1)
            self.quick_sort(partition + 1, hi)

    def partition(self, lo, hi):
        '''
        重新排序数列，所有比基准值小的元素摆放在基准前面，所有比基准值大的元素摆在基准后面（与基准值相等的数可以到任何一边）。在这个分割结束之后，对基准值的排序就已经完成;获取临界值变换位置后所在的索引
        :param lo:
        :param hi:
        :return:
        '''

        # 定义分界值 默认第一个元素
        pivot = self.arr[hi]
        # 定义最小元素索引
        i = lo - 1
        for j in range(lo, hi):
            if self.arr[j] < pivot:
                i += 1
                self.exch_(i, j)

        self.exch_(i+1, hi)
        return i+1

    def less_(self, num1, num2):
        '''
        判断小于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 < num2

    def exch_(self, index1, index2):
        '''
        交换位置
        :param arr:
        :param index1:
        :param index2:
        :return:
        '''
        self.arr[index1], self.arr[index2] = self.arr[index2], self.arr[index1]


if __name__ == '__main__':
    arr = [6, 1, 2, 7, 9, 3, 4, 5, 8]
    sort_ = Quick(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)