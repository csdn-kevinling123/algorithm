# -*- coding: utf-8 -*-


class Insertion(object):
    '''
    插入排序
    1. 将待排序的组分为 已排序 和 未排序
    2. 从未排序中取出第一个值，向已排序的组中进行插入
    3. 倒叙遍历已排序的元素，依次和待插入的元素进行比较，若大于则交换位置，直到找到一个元素小于待插入的元素
    '''

    def __init__(self, arr: list):
        self.arr = arr

    def sort_(self):
        '''
        排序
        :param arr:
        :return:
        '''
        n = len(self.arr)
        for i in range(1, n):
            j = i - 1
            # while j >= 0 and val < arr[j]:
            #     self.exch_(arr=arr, index1=j, index2=j+1)
            #     j -= 1

            while j >= 0:
                # 比较次数：(n-1) + (n-2) + (n-3) + 2 + 1 = n^2 / 2 - n / 2
                if self.greate_(num1=self.arr[j], num2=self.arr[j+1]):
                    # 交换次数：(n-1) + (n-2) + (n-3) + 2 + 1 = n^2 / 2 - n / 2
                    self.exch_(index1=j, index2=j+1)
                else:
                    break
                j -= 1
        # 总执行次数： (n^2 / 2 - n / 2) + ( n^2 / 2 - n / 2) = n^2 - n
        # 根据大O推到法则，插入排序的时间复杂度为： O(n^2)
        return self.arr

    def greate_(self, num1, num2):
        '''
        判断大于
        :param num1:
        :param num2:
        :return:
        '''
        return num1 > num2

    def exch_(self, index1, index2):
        '''
        交换位置
        :param arr:
        :param index1:
        :param index2:
        :return:
        '''
        temp = self.arr[index1]
        self.arr[index1] = self.arr[index2]
        self.arr[index2] = temp


if __name__ == '__main__':
    arr = [64, 34, 25, 12, 22, 11, 90]
    sort_ = Insertion(arr=arr)
    sorted_arr = sort_.sort_()
    print(sorted_arr)