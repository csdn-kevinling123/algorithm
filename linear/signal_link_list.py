# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.item = item
        self.next = next


class SignalLinkList(object):
    '''
    单向链表
    1. 清空单向链表
    2. 判断单向链表是否为空
    3. 获取单向链表元素的个数
    4. 返回线单向链表第i个的值
    5. 向单向链表的指定位置插入元素
    6. 在单向链表的最后末尾插入一个元素
    7. 删除单向链表的第i个元素
    8. 获取单向链表中某个元素第一次出现的位置
    '''

    def __init__(self):
        # 初始化头节点
        self.head = LinkNode(item=None, next=None)
        self.len_ = 0

    def clear_(self):
        self.head.next = None
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def insert_(self, val):
        '''
        在末尾插入节点
        :param val:
        :return:
        '''
        n = self.head
        while n.next:
            n = n.next
        new_node = LinkNode(item=val, next=None)
        n.next = new_node
        self.len_ += 1

    def insert_i(self, val, i):
        '''
        在指定位置插入节点
        :param val:
        :param i:
        :return:
        '''
        if i > self.len_:
            i = self.len_
        pre_node = self.head
        j = 0
        while j < i:
            pre_node = pre_node.next
            j += 1
        # 原i处的节点
        cur_node = pre_node.next
        # 插入节点
        new_node = LinkNode(item=val, next=cur_node)
        pre_node.next = new_node
        self.len_ += 1

    def remove_i(self, i):
        '''
        删除指定位置的节点
        :param i:
        :return:
        '''
        if i > self.len_:
            i = self.len_
        pre_node = self.head
        j = 0
        while j < i:
            pre_node = pre_node.next
            j += 1
        cur_node = pre_node.next
        if not cur_node.next:
            pre_node.next = None
        else:
            pre_node.next = cur_node.next
        self.len_ -= 1
        return cur_node.item

    def index_of(self, val):
        n = self.head
        i = 0
        j = -1
        while n.next:
            n = n.next
            if n.item == val:
                j = i
                break
            i += 1
        return j

    def get_(self):
        link_list = []
        n = self.head
        while n.next:
            n = n.next
            link_list.append(n.item)
        return link_list

    def reverse(self):
        '''
        反转链表
        :return:
        '''
        if self.is_empty():
            return None
        self.reverse_node(self.head.next)

    def reverse_node(self, node):
        '''
        节点反转
        :param node:
        :return:
        '''
        if node.next:
            pre = self.reverse_node(node.next)
            pre.next = node
            node.next = None
        else:
            self.head.next = node
        return node

    def get_mid(self):
        '''
        快慢指针找中间值
        :return:
        '''
        fast = self.head
        slow = self.head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        return slow.item

    def is_circle_link(self):
        '''
        快慢指针判断是否有环链表
        :return:
        '''
        fast = self.head
        slow = self.head
        is_circle = False
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
            if slow == fast:
                is_circle = True
                break
        return is_circle

    def get_circle_link_enter(self):
        '''
        快慢指针找到有环链表的入口
        :return:
        '''
        fast = self.head
        slow = self.head
        temp = None
        circle_index = -1
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
            if circle_index > -1:
                temp = temp.next
            if temp == slow:
                break
            if slow == fast:
                circle_index += 1
                temp = self.head
        return [circle_index, slow]


if __name__ == '__main__':
    link_list = SignalLinkList()
    link_list.insert_(1)
    link_list.insert_(2)
    link_list.insert_(3)
    link_list.insert_(4)
    links = link_list.get_()
    print(links)

    # link_list.insert_i('a', 2)
    # links = link_list.get_()
    # print(links)

    links = link_list.reverse()
    links = link_list.get_()
    print(links)

    mid = link_list.get_mid()
    print('mid:%s' % mid)


    val_index = link_list.index_of(3)
    print(val_index)
    print(link_list.get_len())

    val_index = link_list.index_of(10)
    print(val_index)

    link_list.remove_i(1)
    links = link_list.get_()
    print(links)
    print(link_list.get_len())





