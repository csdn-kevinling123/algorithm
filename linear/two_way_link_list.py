# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None, pre=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        :param pre: 前一节点
        '''
        self.item = item
        self.next = next
        self.pre = pre


class TwoWayLinkList(object):
    '''
    双向链表
    1. 清空双向链表
    2. 判断双向链表是否为空
    3. 获取双向链表元素的个数
    4. 返回线双向链表第i个的值
    5. 向双向链表的指定位置插入元素
    6. 在双向链表的最后末尾插入一个元素
    7. 删除双向链表的第i个元素
    8. 获取双向链表中某个元素第一次出现的位置
    9. 获取第一个节点的元素
    10. 获取最后一个节点的元素
    '''

    def __init__(self):
        # 初始化头节点
        self.head = LinkNode(item=None, next=None)
        self.last = None
        self.len_ = 0

    def clear_(self):
        self.head.next = None
        self.last = None
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def get_first(self):
        if self.is_empty():
            return None
        n = self.head.next
        return n.item

    def get_last(self):
        if self.is_empty():
            return None
        return self.last.item

    def insert_(self, val):
        '''
        在末尾插入节点
        :param val:
        :return:
        '''
        if self.is_empty():
            new_node = LinkNode(item=val, pre=self.head, next=None)
            self.head.next = new_node
        else:
            old_last =self.last
            new_node = LinkNode(item=val, pre=old_last, next=None)
            old_last.next = new_node
        self.last = new_node
        self.len_ += 1

    def insert_i(self, val, i):
        '''
        在指定位置插入节点
        :param val:
        :param i:
        :return:
        '''
        if i > self.len_:
            i = self.len_
        pre_node = self.head
        j = 0
        while j < i:
            pre_node = pre_node.next
            j += 1
        # 原i处的节点
        cur_node = pre_node.next
        # 插入节点
        new_node = LinkNode(item=val, pre=pre_node, next=cur_node)
        pre_node.next = new_node
        cur_node.pre = new_node
        self.len_ += 1

    def remove_i(self, i):
        '''
        删除指定位置的节点
        :param i:
        :return:
        '''
        if i > self.len_:
            i = self.len_
        pre_node = self.head
        j = 0
        while j < i:
            pre_node = pre_node.next
            j += 1
        cur_node = pre_node.next
        if not cur_node.next:
            pre_node.next = None
            cur_node.pre = None
            self.last = pre_node
        else:
            next_node = cur_node.next
            pre_node.next = next_node
            next_node.pre = pre_node
            cur_node.pre = None
            cur_node.next = None
        self.len_ -= 1
        return cur_node.item

    def get_i(self, i):
        if self.is_empty():
            return None
        cur_node = self.head
        j = 0
        while j < i:
            cur_node = cur_node.next
            j += 1
        return cur_node.item

    def index_of(self, val):
        n = self.head
        i = 0
        j = -1
        while n.next:
            n = n.next
            if n.item == val:
                j = i
                break
            i += 1
        return j

    def get_pre(self):
        link_list = []
        n = self.head
        while n.next:
            n = n.next
            link_list.append(n.item)
        return link_list

    def get_last(self):
        link_list = []
        n = self.last
        while n.pre:
            link_list.append(n.item)
            n = n.pre
        return link_list


if __name__ == '__main__':
    link_list = TwoWayLinkList()
    link_list.insert_(1)
    link_list.insert_(2)
    link_list.insert_(3)
    link_list.insert_(4)
    links = link_list.get_pre()
    print(links)
    links = link_list.get_last()
    print(links)

    link_list.insert_i('a', 2)
    links = link_list.get_pre()
    print(links)
    links = link_list.get_last()
    print(links)

    val_index = link_list.index_of(3)
    print(val_index)
    print(link_list.get_len())

    val_index = link_list.index_of(10)
    print(val_index)

    link_list.remove_i(1)
    links = link_list.get_pre()
    print(links)
    links = link_list.get_last()
    print(links)
    print(link_list.get_len())





