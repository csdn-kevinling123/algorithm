# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.item = item
        self.next = next


class QueueW(object):
    '''
    队列
    '''
    def __init__(self):
        self.head = LinkNode(item=None, next=None)
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def push_(self, val):
        '''
        入队
        :param val:
        :return:
        '''
        new_node = LinkNode(item=val, next=None)
        if self.is_empty():
            self.head.next = new_node
        else:
            next_node = self.head.next
            while next_node.next:
                next_node = next_node.next
            next_node.next = new_node
        self.len_ += 1

    def pop_(self):
        '''
        出队
        :return:
        '''
        exit_node = self.head.next
        if not exit_node:
            return None
        self.head.next = exit_node.next
        self.len_ -= 1
        return exit_node.item

    def get_(self):
        '''
        获取栈的元素
        :return:
        '''
        curr_node = self.head.next
        if not curr_node:
            return []
        node_list = []
        while curr_node.next:
            node_list.append(curr_node.item)
            curr_node = curr_node.next
        node_list.append(curr_node.item)
        return node_list


if __name__ == '__main__':
    queue_w = QueueW()
    queue_w.push_('a')
    queue_w.push_('b')
    queue_w.push_('c')
    queue_w.push_('d')
    node_list = queue_w.get_()
    print(node_list)

    queue_w.pop_()
    node_list = queue_w.get_()
    print(node_list)
