# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, key=None, value=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.key = key
        self.value = value
        self.next = next


class SymbolTable(object):
    '''
    符号表
    '''
    def __init__(self):
        self.head = LinkNode(key=None, value=None, next=None)
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def put_(self, key, val):
        '''
        添加元素
        :param val:
        :return:
        '''
        curr_node = self.head
        is_save = False
        while curr_node.next:
            curr_node = curr_node.next
            if curr_node.key == key:
                curr_node.value = val
                is_save = True
                break
        if not is_save:
            new_node = LinkNode(value=val, key=key, next=None)
            curr_node.next = new_node
            self.len_ += 1

    def del_(self, key):
        '''
        删除元素
        :return:
        '''
        curr_node = self.head
        pre_node = None
        is_save = False
        while curr_node.next:
            pre_node = curr_node
            curr_node = curr_node.next
            if curr_node.key == key:
                pre_node.next = curr_node.next
                self.len_ -= 1
                is_save = True
                break

        if is_save:
            return curr_node.value
        else:
            return False

    def get_(self, key):
        '''
        获取元素
        :return:
        '''
        curr_node = self.head
        is_save = False
        while curr_node.next:
            curr_node = curr_node.next
            if curr_node.key == key:
                is_save = True
                break
        if is_save:
            return curr_node.value
        else:
            return None


if __name__ == '__main__':
    symbol_table = SymbolTable()
    symbol_table.put_(key='a', val=1)
    symbol_table.put_(key='b', val=2)
    symbol_table.put_(key='c', val=3)
    symbol_table.put_(key='d', val=4)
    node_list = symbol_table.get_(key='b')
    print(node_list)
    node_list = symbol_table.get_(key='d')
    print(node_list)
    node_list = symbol_table.get_(key='e')
    print(node_list)

    node_list = symbol_table.del_(key='c')
    print(node_list)
