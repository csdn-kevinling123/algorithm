# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.item = item
        self.next = next


class Joseph(object):
    '''
    约瑟夫问题
    '''

    def __init__(self, num):
        self.head = None
        # 构建循环链表
        for i in range(num):
            if i == 0:
                self.head = LinkNode(item=i+1, next=None)
                curr_node = self.head
                continue
            node = LinkNode(item=i+1, next=None)
            curr_node.next = node
            curr_node = node
            if i == num-1:
                curr_node.next = self.head

        # 遍历循环链表
        self.count = 0
        curr_node = self.head # 当前节点
        pre_node = None # 当前节点的上一节点
        while curr_node != curr_node.next:
            self.count += 1
            print('count:%s' % self.count)
            if self.count == 3:
                # 剔除当前节点
                pre_node.next = curr_node.next
                print('cur_item:%s' % curr_node.item)
                self.count = 0
            else:
                pre_node = curr_node
            curr_node = curr_node.next
        print('last:%s' % curr_node.item)


if __name__ == '__main__':
    link_list = Joseph(41)


