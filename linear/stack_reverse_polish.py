# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.item = item
        self.next = next


class Stack(object):
    '''
    用链表实现栈
    '''
    def __init__(self):
        self.head = LinkNode(item=None, next=None)
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def push_(self, val):
        '''
        入栈
        :param val:
        :return:
        '''
        next_node = self.head.next
        new_node = LinkNode(item=val, next=next_node)
        self.head.next = new_node
        self.len_ += 1

    def pop_(self):
        '''
        出栈
        :return:
        '''
        exit_node = self.head.next
        if not exit_node:
            return None
        self.head.next = exit_node.next
        self.len_ -= 1
        return exit_node.item

    def get_(self):
        '''
        获取栈的元素
        :return:
        '''
        curr_node = self.head.next
        node_list = []
        while curr_node.next:
            node_list.append(curr_node.item)
            curr_node = curr_node.next
        node_list.append(curr_node.item)
        return node_list


class ReversePolish(object):
    '''
    逆波兰表达式
    '''

    def cul(self, list_: list):
        stack = Stack()
        for i in list_:
            if i in ["-", "*", "/", "+"]:
                item2 = stack.pop_()
                item1 = stack.pop_()
                item = self.cul_type(item1=item1, item2=item2, type=i)
                stack.push_(item)
            else:
                stack.push_(i)
        return stack.pop_()

    def cul_type(self, item1, item2, type):
        if type == '+':
            item = item1 + item2
        elif type == '-':
            item = item1 - item2
        elif type == '*':
            item = item1 * item2
        elif type == '/':
            item = item1 / item2
        else:
            item = 0
        return item


if __name__ == '__main__':
    reverse_polish = ReversePolish()
    list_ = [3, 17, 15, "-", "*", 18, 6, "/", "+"]
    res = reverse_polish.cul(list_=list_)
    print(res)
