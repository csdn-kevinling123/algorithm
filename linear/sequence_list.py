# -*- coding: utf-8 -*-


class SequenceList(object):
    '''
    顺序线性表
    1. 清空线性表
    2. 判断线性表是否为空
    3. 获取线性表元素的个数
    4. 返回线性表第i个的值
    5. 想线性表的指定位置插入元素
    6. 在线性表的最后末尾插入一个元素
    7. 删除线性表的第i个元素
    8. 获取线性表中某个元素第一次出现的位置
    9. 获取线性表
    '''

    def __init__(self):
        self.list_ = []
        self.len_ = 0

    def clear_(self):
        if self.len_:
            for i in range(0, self.len_):
                self.list_.pop(0)
        self.list_ = []
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def insert_(self, val):
        self.list_.append(val)
        self.len_ += 1

    def insert_i(self, val, i):
        if i > self.len_:
            i = self.len_
        j = self.len_ - 1
        self.list_.append(0)
        while i <= j:
            self.list_[j+1] = self.list_[j]
            j -= 1
        self.list_[i] = val
        self.len_ += 1

    def remove_i(self, i):
        if i < 0:
            i = 0
        if i >= self.len_:
            i = self.len_ - 1
        val = self.list_.pop(i)
        self.len_ -= 1
        return val

    def index_of(self, val):
        j = -1
        for i in range(self.len_):
            if val == self.list_[i]:
                j = i
                break
        return j

    def get_(self):
        return self.list_


if __name__ == '__main__':
    liner_list = SequenceList()
    liner_list.insert_('a')
    liner_list.insert_('b')
    liner_list.insert_('c')
    liner_list.insert_('d')
    liner_list.insert_('e')
    print('当前线性表：')
    print(liner_list.get_())
    liner_list.insert_i('cc', 2)
    print('在第三个位置插入cc后的线性表：')
    print(liner_list.get_())

    len_ = liner_list.get_len()
    print('当前线性表的长度：')
    print(len_)

    val = liner_list.remove_i(1)
    print('删除第二个位置元素后的线性表：')
    print(val)

    print('当前线性表：')
    print(liner_list.get_())
    index_of_d = liner_list.index_of('d')
    print('d元素的索引：')
    print(index_of_d)

    print(liner_list.is_empty())
    liner_list.clear_()
    print(liner_list.is_empty())