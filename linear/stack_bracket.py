# -*- coding: utf-8 -*-


class LinkNode(object):
    '''
    链表节点
    '''
    def __init__(self, item=None, next=None):
        '''
        节点信息
        :param item: 数据
        :param next: 下一节点
        '''
        self.item = item
        self.next = next


class Stack(object):
    '''
    用链表实现栈
    '''
    def __init__(self):
        self.head = LinkNode(item=None, next=None)
        self.len_ = 0

    def is_empty(self):
        return self.len_ == 0

    def get_len(self):
        return self.len_

    def push_(self, val):
        '''
        入栈
        :param val:
        :return:
        '''
        next_node = self.head.next
        new_node = LinkNode(item=val, next=next_node)
        self.head.next = new_node
        self.len_ += 1

    def pop_(self):
        '''
        出栈
        :return:
        '''
        exit_node = self.head.next
        if not exit_node:
            return None
        self.head.next = exit_node.next
        self.len_ -= 1
        return exit_node.item

    def get_(self):
        '''
        获取栈的元素
        :return:
        '''
        curr_node = self.head.next
        node_list = []
        while curr_node.next:
            node_list.append(curr_node.item)
            curr_node = curr_node.next
        node_list.append(curr_node.item)
        return node_list


class Bracket(object):
    '''
    括号匹配问题
    '''

    def match_str(self, str_: str):
        stack = Stack()
        return self._do_match(str_, stack)

    def _do_match(self, str_: str, stack):
        is_right = True
        for i in str_:
            if i == '(':
                stack.push_(i)
            elif i == ')':
                item = stack.pop_()
                if not item:
                    is_right = False
                    break
        return is_right


if __name__ == '__main__':
    bracket = Bracket()
    res = bracket.match_str('safa(爱上)凤(凰凯)撒q)eqwe')
    print(res)

